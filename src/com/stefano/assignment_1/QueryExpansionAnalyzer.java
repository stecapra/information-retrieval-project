package com.stefano.assignment_1;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.standard.ClassicTokenizer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.synonym.SynonymFilter;
import org.apache.lucene.analysis.synonym.SynonymMap;
import org.apache.lucene.analysis.synonym.WordnetSynonymParser;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.*;
import java.text.ParseException;

public class QueryExpansionAnalyzer extends Analyzer {

    private static final String wn_pl_path = "wn/wn_s.pl";

    public void printSyn(String word) throws IOException {
        TokenStreamComponents TSC = createComponents( "" , new StringReader(word));
        TokenStream stream = TSC.getTokenStream();
        CharTermAttribute termattr = stream.addAttribute(CharTermAttribute.class);
        stream.reset();
        while (stream.incrementToken()) {
            System.out.println(termattr.toString());
        }
    }

    private SynonymMap buildSynonym() throws IOException, ParseException {
        File file = new File(wn_pl_path);
        InputStream stream = new FileInputStream(file);
        Reader rulesReader = new InputStreamReader(stream);
        WordnetSynonymParser parser = new WordnetSynonymParser(true, true, new StandardAnalyzer(CharArraySet.EMPTY_SET));
        parser.parse(rulesReader);
        return parser.build();
    }

    protected TokenStreamComponents createComponents(String string, Reader reader)
    {
        Tokenizer source = new ClassicTokenizer();
        source.setReader(reader);
        TokenStream filter = new StandardFilter(source);

        filter = new LowerCaseFilter(filter);
        SynonymMap mySynonymMap = null;
        try {
            mySynonymMap = buildSynonym();
        } catch (IOException | ParseException e) { e.printStackTrace(); }

        filter = new SynonymFilter(filter, mySynonymMap, false);

        return new TokenStreamComponents(source, filter);
    }

    @Override
    protected TokenStreamComponents createComponents(String string)
    {
        Tokenizer source = new ClassicTokenizer();
        TokenStream filter = new StandardFilter(source);

        filter = new LowerCaseFilter(filter);
        SynonymMap mySynonymMap = null;
        try {
            mySynonymMap = buildSynonym();
        } catch (IOException | ParseException e) { e.printStackTrace(); }

        filter = new SynonymFilter(filter, mySynonymMap, false);

        return new TokenStreamComponents(source, filter);
    }
}