package com.stefano.assignment_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

/** Simple command-line based search demo. */
public class SearchFilesRocchio {

    private SearchFilesRocchio() {}

    /** Simple command-line based search demo. */
    public static void main(String[] args) throws Exception {
        String usage =
                "Usage:\tjava org.apache.lucene.demo.SearchFilesModule [-index dir] [-field f] [-repeat n] [-queries file] [-query string] [-raw] [-paging hitsPerPage]\n\nSee http://lucene.apache.org/core/4_1_0/demo/ for details.";
        if (args.length > 0 && ("-h".equals(args[0]) || "-help".equals(args[0]))) {
            System.out.println(usage);
            System.exit(0);
        }
        String index = "index";
        String field = "contents";
        String queries = null;
        int repeat = 0;
        boolean raw =false;
        String queryString = null;
        int hitsPerPage = 10;

        for(int i = 0;i < args.length;i++) {
            if ("-index".equals(args[i])) {
                index = args[i+1];
                i++;
            } else if ("-field".equals(args[i])) {
                field = args[i+1];
                i++;
            } else if ("-queries".equals(args[i])) {
                queries = args[i+1];
                i++;
            } else if ("-query".equals(args[i])) {
                queryString = args[i+1];
                i++;
            } else if ("-repeat".equals(args[i])) {
                repeat = Integer.parseInt(args[i+1]);
                i++;
            } else if ("-raw".equals(args[i])) {
                raw = true;
            } else if ("-paging".equals(args[i])) {
                hitsPerPage = Integer.parseInt(args[i+1]);
                if (hitsPerPage <= 0) {
                    System.err.println("There must be at least 1 hit per page.");
                    System.exit(1);
                }
                i++;
            }
        }

        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
        IndexSearcher searcher = new IndexSearcher(reader);
        Analyzer analyzer = new StandardAnalyzer();
        //Analyzer analyzer = new QueryExpansionAnalyzer();
        BufferedReader in = null;
        if (queries != null) {
            in = Files.newBufferedReader(Paths.get(queries), StandardCharsets.UTF_8);
        } else {
            in = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
        }
        QueryParser parser = new QueryParser(field, analyzer);
        while (true) {
            if (queries == null && queryString == null) {                        // prompt the user
                System.out.println("Enter query: ");
            }

            String line = queryString != null ? queryString : in.readLine();

            if (line == null || line.length() == -1) {
                break;
            }

            line = line.trim();
            if (line.length() == 0) {
                break;
            }

            Query query = parser.parse(line);

            // Decomment to use wildcard queries
            /*Term term = new Term("contents", line);
            query = new WildcardQuery(term);
            System.out.println("Searching for: " + query.toString(field));*/

            if (repeat > 0) {                           // repeat & time as benchmark
                Date start = new Date();
                for (int i = 0; i < repeat; i++) {
                    searcher.search(query, 100);
                }
                Date end = new Date();
                System.out.println("Time: "+(end.getTime()-start.getTime())+"ms");
            }

            doPagingSearch(in, searcher, reader, query, line, hitsPerPage, raw, queries == null && queryString == null,false);

            if (queryString != null) {
                break;
            }
        }
        reader.close();
    }

    /**
     * This demonstrates a typical paging search scenario, where the search engine presents
     * pages of size n to the user. The user can then go to the next page if interested in
     * the next hits.
     *
     * When the query is executed for the first time, then only enough results are collected
     * to fill 5 result pages. If the user wants to page beyond this limit, then the query
     * is executed another time and all hits are collected.
     *
     */
    private static void doPagingSearch(BufferedReader in, IndexSearcher searcher, IndexReader indexReader, Query query,
                                       String originalQueryText, int hitsPerPage, boolean raw, boolean interactive,boolean newTermQuery) throws IOException, ParseException {

        double alpha = 1;
        double beta = 0.8;
        int kRelevant = 5;
        int kNotRelevant = 0;

        if(!newTermQuery) {
            String [] queryTerms = originalQueryText.split(" ");
            Map<String,Long> queryVocabulary = Arrays.stream(queryTerms).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            // Collect enough docs to show 5 pages
            TopDocs results = searcher.search(query, 5 * hitsPerPage);
            ScoreDoc[] hits = results.scoreDocs;
            int hitsLength = results.scoreDocs.length;

            if (hitsLength < kRelevant) kRelevant = hitsLength;
            else kNotRelevant = hitsLength - kRelevant;

            alpha = alpha / kRelevant;
            if(kNotRelevant > 0) beta = beta / kNotRelevant;

            List<List<String>> docs = new ArrayList<>();
            for (int i = 0; i < hitsLength; i++) {
                TermsEnum terms = indexReader.getTermVectors(results.scoreDocs[i].doc).terms("contents").iterator();
                BytesRef term = terms.next();
                List<String> l = new ArrayList<>();
                while (term != null) {
                    String termString = term.utf8ToString();
                    l.add(termString);

                    term = terms.next();
                }

                docs.add(l);
            }

            HashMap<String, Double> relevantVocabulary = new HashMap<>();
            HashMap<String, Double> notRelevantVocabulary = new HashMap<>();
            for (int i = 0; i < hitsLength; i++) {
                TermsEnum terms = indexReader.getTermVectors(results.scoreDocs[i].doc).terms("contents").iterator();
                BytesRef term = terms.next();
                while (term != null) {
                    String termString = term.utf8ToString();

                    Double prevScore = relevantVocabulary.get(termString);
                    if (i > kRelevant) prevScore = notRelevantVocabulary.get(termString);
                    if (prevScore == null) prevScore = 0.0;

                    TFIDFCalculator calculator = new TFIDFCalculator();
                    double tfidf = calculator.tfIdf(docs.get(i), docs, termString);

                    Double score = prevScore + tfidf;

                    if(i > kRelevant) notRelevantVocabulary.put(termString, score);
                    else relevantVocabulary.put(termString, score);

                    term = terms.next();
                }
            }

            Map<String, Double> fin = new HashMap<>();
            for (Object o : queryVocabulary.entrySet()) {
                Map.Entry pair = (Map.Entry) o;
                String key = pair.getKey().toString();

                Long value = (Long) pair.getValue();

                Double notRelevantValue = notRelevantVocabulary.get(key);
                Double relevantValue = relevantVocabulary.get(key);

                if(notRelevantValue == null) notRelevantValue = 0.0;
                if(relevantValue == null) relevantValue = 0.0;

                Double v = value + alpha * relevantValue - beta * notRelevantValue;

                fin.put(key, v);
            }

            for (Object o : relevantVocabulary.entrySet()) {
                Map.Entry pair = (Map.Entry) o;
                String key = pair.getKey().toString();
                if(fin.get(key) == null) {
                    Double value = (Double) pair.getValue();

                    Double notRelevantValue = notRelevantVocabulary.get(key);
                    Long queryValue = queryVocabulary.get(key);

                    if(notRelevantValue == null) notRelevantValue = 0.0;
                    if(queryValue == null) queryValue = 0L;

                    Double v = queryValue + alpha * value - beta * notRelevantValue;

                    fin.put(key, v);
                }
            }

            for (Object o : notRelevantVocabulary.entrySet()) {
                Map.Entry pair = (Map.Entry) o;
                String key = pair.getKey().toString();
                if (fin.get(key) == null) {
                    Double value = (Double) pair.getValue();

                    Double relevantValue = relevantVocabulary.get(key);
                    Long queryValue = queryVocabulary.get(key);

                    if (relevantValue == null) relevantValue = 0.0;
                    if (queryValue == null) queryValue = 0L;

                    Double v = queryValue + alpha * relevantValue - beta * value;

                    fin.put(key, v);
                }
            }

            int kExpansionTerms = 5;

            List<String> newQuery = new ArrayList<>();
            int j = 0;
            int finLength = fin.size();
            try {
                while (newQuery.size() < kExpansionTerms && j <= finLength) {
                    String max = fin.entrySet().stream().max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).get().getKey();
                    if (newQuery.indexOf(max) == -1) newQuery.add(max);
                    fin.remove(max);

                    j++;
                }
            } catch (Exception e) {
                //
            }
            String joined = String.join(" ", newQuery);
            System.out.println("New query: " + joined);
            String field = "contents";
            Analyzer analyzer = new StandardAnalyzer();
            QueryParser parser = new QueryParser(field, analyzer);
            Query newQueryString = parser.parse(joined);
            doPagingSearch(in,searcher,indexReader,newQueryString,originalQueryText,hitsPerPage,raw,interactive,true);
        } else {
            TopDocs results = searcher.search(query, 5 * hitsPerPage);
            ScoreDoc[] hits = results.scoreDocs;
            int numTotalHits = Math.toIntExact(results.totalHits);
            System.out.println(numTotalHits + " total matching documents");

            int start = 0;
            int end = Math.min(numTotalHits, hitsPerPage);

            while (true) {
                if (end > hits.length) {
                    System.out.println("Only results 1 - " + hits.length + " of " + numTotalHits + " total matching documents collected.");
                    System.out.println("Collect more (y/n) ?");
                    String line = in.readLine();
                    if (line.length() == 0 || line.charAt(0) == 'n') {
                        break;
                    }

                    hits = searcher.search(query, numTotalHits).scoreDocs;
                }

                end = Math.min(hits.length, start + hitsPerPage);

                for (int i = start; i < end; i++) {
                    if (raw) {                              // output raw format
                        System.out.println("doc=" + hits[i].doc + " score=" + hits[i].score);
                        continue;
                    }

                    Document doc = searcher.doc(hits[i].doc);
                    String path = doc.get("path");
                    if (path != null) {
                        System.out.println((i + 1) + ". " + path);
                        String title = doc.get("title");
                        if (title != null) {
                            System.out.println("   Title: " + doc.get("title"));
                        }
                    } else {
                        System.out.println((i + 1) + ". " + "No path for this document");
                    }

                }

                if (!interactive || end == 0) {
                    break;
                }

                if (numTotalHits >= end) {
                    boolean quit = false;
                    while (true) {
                        System.out.print("Press ");
                        if (start - hitsPerPage >= 0) {
                            System.out.print("(p)revious page, ");
                        }
                        if (start + hitsPerPage < numTotalHits) {
                            System.out.print("(n)ext page, ");
                        }
                        System.out.println("(q)uit or enter number to jump to a page.");

                        String line = in.readLine();
                        System.out.print(line);
                        if (line.length() == 0 || line.charAt(0) == 'q') {
                            quit = true;
                            break;
                        }
                        if (line.charAt(0) == 'p') {
                            start = Math.max(0, start - hitsPerPage);
                            break;
                        } else if (line.charAt(0) == 'n') {
                            if (start + hitsPerPage < numTotalHits) {
                                start += hitsPerPage;
                            }
                            break;
                        } else {
                            int page = Integer.parseInt(line);
                            if ((page - 1) * hitsPerPage < numTotalHits) {
                                start = (page - 1) * hitsPerPage;
                                break;
                            } else {
                                System.out.println("No such page");
                            }
                        }
                    }
                    if (quit) break;
                    end = Math.min(numTotalHits, start + hitsPerPage);
                }
            }
        }
    }
}
